﻿using System;
using System.Collections.Specialized;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            NameValueCollection nvc = ReadSettings();
            Console.WriteLine("Hello World!");
            Console.WriteLine(""+ nvc["RequestTimeOut"]);
            Console.WriteLine("" + nvc["OfflineDataRefreshTime"]);
            Console.WriteLine("" + nvc["BaseUrlPort"]);

        }

                //17/Decemeber/2019
       static NameValueCollection ReadSettings()
        {
            NameValueCollection nvc = new NameValueCollection();

            try
            {
                System.IO.StreamReader reader = new System.IO.StreamReader(Environment.CurrentDirectory+ "\\DataServiceSettings.sys");

                string data = reader.ReadToEnd();
                char[] spl = new char[1];
                spl[0] = ';';
                string[] spltData = data.Split(spl);
                 
                foreach(string str in spltData)
                {
                    if (str.IndexOf(":") > -1)
                    {
                        string key = str.Substring(0,str.IndexOf(":")).Replace(":","");
                        string value = str.Substring(str.IndexOf(":")).Replace(":", "");
                        nvc.Add(key, value);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return nvc;
        }
        //
    }
}
